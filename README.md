## Interview Testing

# Preparing

```bash
  - Instal packages
    $ npm install
```

# Running server

```bash
  - Instal packages
    $ npm run start
```

# Swagger documentation

- http://localhost:3000/api

# Login accounts

```bash
  1.
    email: account1@gmail.com
    password: @password@

  2.
    email: account2@gmail.com
    password: @password@

  3.
    email: account3@gmail.com
    password: @password@

  4.
    email: account4@gmail.com
    password: @password@

  5.
    email: account5@gmail.com
    password: @password@
```

# Debugging

- In .vscode folder, create launch.json file
- Adding the below content to launch.json

```bash
  {
    "version": "0.2.0",
    "configurations": [
      {
        "name": "NestJS debug",
        "type": "node",
        "request": "launch",
        "args": ["${workspaceFolder}/src/main.ts"],
        "runtimeArgs": [
          "--nolazy",
          "-r",
          "ts-node/register",
          "-r",
          "tsconfig-paths/register"
        ],
        "sourceMaps": true,
        "cwd": "${workspaceRoot}",
        "protocol": "inspector",
        "console": "integratedTerminal",
        "env": {
          "NODE_ENV": "local"
        }
      }
    ]
  }
```
