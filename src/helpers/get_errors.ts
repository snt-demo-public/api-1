import { notFoundErrors, badRequestErrors } from '../constants/errors'

export const getErrors = (error) => {
  const statusCodes = [error.status, error.code, error.statusCode]

  if (statusCodes.includes(401)) {
    return {
      error: 'Unauthorized',
      code: 401,
    }
  }

  if (statusCodes.includes(11000) && error.name === 'MongoError') {
    return {
      message: 'Duplicated email, name or phone',
      error: 'Bad Request',
      code: 400,
    }
  }

  if (statusCodes.includes(404)) {
    return {
      message: notFoundErrors[error.name] || error.name,
      code: 404,
      error: 'Not Found',
    }
  }

  if (statusCodes.includes(400)) {
    return {
      message: badRequestErrors[error.name] || error.name || error.message,
      error: 'Bad Request',
      code: 400,
    }
  }

  return {
    message: `${error.message} - ${error.stack}`,
    code: 500,
    error: 'Internal Server Error',
  }
}
