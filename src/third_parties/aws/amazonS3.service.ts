import { Injectable } from '@nestjs/common'
import * as AWS from 'aws-sdk'
import { ConfigService } from '../../configs/configs.service'
import { extname } from 'path'

const configService = new ConfigService()

@Injectable()
export class AwsService {
  private s3bucket = new AWS.S3({
    accessKeyId: configService.awsAccessKey,
    secretAccessKey: configService.awsSecretAccessKey,
  })

  private defaultURL = `http://s3.${configService.awsRegion}.amazonaws.com/${configService.awsBucket}/`

  upload({ fileName, buffer, fileType, fileSize }) {
    return new Promise((resolve, reject) => {
      const randomName = Array(32).fill(null).map(() =>
        (Math.round(Math.random() * 16)).toString(16)).join('')

      const s3Link = `${configService.awsFolder}/${randomName}${Date.now()}${extname(fileName)}`

      this.s3bucket.upload({
        ACL: 'public-read',
        Body: buffer,
        Key: s3Link,
        Bucket: configService.awsBucket,
      }, (err, data) => {
        if (err) {
          reject(err)
        }

        resolve({
          ...data,
          s3Link: `${this.defaultURL}${s3Link}`,
          fileSize,
          fileType,
          fileName,
        })
      })
    })
  }

  verifyFileInStorage(Key: string) {
    const comparingString = Key.substr(0, this.defaultURL.length)

    if (comparingString === this.defaultURL) {
      Key = Key.replace(comparingString, '')
    }

    if (Key[0] === '/') {
      Key = Key.substr(1)
    }

    return new Promise((resolve, reject) => {
      this.s3bucket.headObject({
        Key,
        Bucket: configService.awsBucket,
      }, (err, data) => {
        if (err) {
          resolve(false)
        }

        resolve(true)
      })
    })
  }

  deleteOne(Key) {
    return new Promise((resolve, reject) => {
      const comparingString = Key.substr(0, this.defaultURL.length)

      if (comparingString === this.defaultURL) {
        Key = Key.replace(comparingString, '')
      }

      this.s3bucket.deleteObject({
        Key,
        Bucket: configService.awsBucket,
      }, (err, data) => {
        if (err) {
          reject(err)
        }

        resolve(data)
      })
    })
  }
}
