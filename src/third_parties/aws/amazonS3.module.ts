import { Module } from '@nestjs/common'
import { AwsService } from './amazonS3.service'

@Module({
  providers: [AwsService],
  exports: [AwsService],
})

export class AwsModule { }
