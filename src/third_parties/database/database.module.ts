import { MongooseModule } from '@nestjs/mongoose'
import { ConfigService } from '../../configs/configs.service'

export const DatabaseModule = MongooseModule.forRootAsync({
  useFactory: async (configService: ConfigService) => {
    return ({
      uri: configService.dbURI,
    })
  },
  inject: [ConfigService],
})
