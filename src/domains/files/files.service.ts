import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import {
  CreateOneFileService,
  DeleteOneFileQuery,
  File,
  FindManyFilesQuery,
  FindManyFilesResult,
  FindOneFileQuery,
  UpdateOneFileService,
} from './types/files.interface'

@Injectable()
export class FilesService {
  constructor(
    @InjectModel('Files')
    private readonly filesModel: Model<File>,
  ) { }

  async createOne(data: CreateOneFileService): Promise<File> {
    try {
      const file = new this.filesModel(data)

      await file.save()

      return file
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async createMany(data: CreateOneFileService[]): Promise<File[]> {
    try {
      if (!data || !data.length) {
        return []
      }

      const files = data.map(each => new this.filesModel(each))

      const insertedFiles = await this.filesModel.insertMany(files)

      return insertedFiles
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findOne(query: FindOneFileQuery): Promise<File> {
    try {
      const file = await this.filesModel.findOne(query).exec()

      if (!file) {
        return Promise.reject({
          name: 'FileNotFound',
          code: 404,
        })
      }

      return file
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateOne({ data, query }: UpdateOneFileService): Promise<File> {
    try {
      const uploadedFile = await this.filesModel.findOneAndUpdate(
        query,
        { $set: data },
        { upsert: true, new: true },
      ).exec()

      return uploadedFile
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async deleteOne(query: DeleteOneFileQuery): Promise<number> {
    try {
      const result = await this.filesModel.deleteOne(query).exec()

      return result.ok
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findMany(query: FindManyFilesQuery): Promise<FindManyFilesResult> {
    try {
      const { offset = 0, limit, ...findingQuery } = query

      const [files, counter] = await Promise.all([
        this.filesModel.find(findingQuery)
          .skip(offset)
          .limit(limit)
          .exec(),
        this.filesModel.count(findingQuery),
      ])

      return {
        data: files,
        totalCount: counter,
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
