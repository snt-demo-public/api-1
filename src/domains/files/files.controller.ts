import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { FilesInterceptor } from '@nestjs/platform-express'
import { ApiConsumes, ApiTags, ApiBody } from '@nestjs/swagger'
import { AwsService } from '../../third_parties/aws/amazonS3.service'
import { ValidationPipe } from '../../middlewares/pipes/validation.pipe'
import { FilesService } from './files.service'
import { FindManyFilesDto, UpdateFileDto } from './types/files.dto'
import { File, FindManyFilesResult, UploadingFile } from './types/files.interface'

@ApiTags('Files Controller')
@Controller('files')
export class FilesController {
  constructor(
    private readonly filesService: FilesService,
    private readonly awsService: AwsService,
  ) { }

  @Post('upload')
  @UseInterceptors(FilesInterceptor('files', 20))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        files: {
          type: 'file[20]',
          maxItems: 20,
          description: 'You can upload maximum 20 files per request',
        },
      },
    },
  })
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async createMany(
    @Request() { user },
    @UploadedFiles() files,
  ): Promise<File[]> {
    try {
      const uploadFilesPromises = files.map((file) => this.awsService.upload({
        fileName: file.originalname,
        fileSize: file.size,
        fileType: file.mimetype,
        buffer: file.buffer,
      }))

      const uploadedFiles: UploadingFile[] = await Promise.all(uploadFilesPromises)

      const createFilesData = uploadedFiles.map(each => ({
        name: each.fileName,
        s3Link: each.s3Link,
        fileType: each.fileType,
        fileSize: each.fileSize,
        createdBy: user.userID,
      }))

      const insertedFiles = await this.filesService.createMany(createFilesData)

      return insertedFiles
    } catch (error) {
      throw error
    }
  }

  @Get(':fileID')
  @UseGuards(AuthGuard('jwt'))
  async findOne(
    @Request() { user },
    @Param() { fileID },
  ): Promise<File> {
    try {
      const file = await this.filesService.findOne({
        _id: fileID,
        createdBy: user.userID,
      })

      return file
    } catch (error) {
      throw error
    }
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async findMany(
    @Request() { user },
    @Query() query: FindManyFilesDto,
  ): Promise<FindManyFilesResult> {
    try {
      const result = await this.filesService.findMany({
        ...query,
        createdBy: user.userID,
      })

      return result
    } catch (error) {
      throw error
    }
  }

  @Put(':fileID')
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async updateOne(
    @Request() { user },
    @Param() { fileID },
    @Body() data: UpdateFileDto,
  ): Promise<File> {
    try {
      const file = await this.filesService.updateOne({
        query: {
          _id: fileID,
          createdBy: user.userID,
        },
        data,
      })

      return file
    } catch (error) {
      throw error
    }
  }

  @Delete(':fileID')
  @UseGuards(AuthGuard('jwt'))
  async deleteOne(
    @Request() { user },
    @Param() { fileID },
  ): Promise<boolean> {
    try {
      const isDeleted = await this.filesService.deleteOne({
        _id: fileID,
        createdBy: user.userID,
      })

      return !!isDeleted
    } catch (error) {
      throw error
    }
  }
}
