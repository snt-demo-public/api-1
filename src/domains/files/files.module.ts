import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { AwsModule } from '../../third_parties/aws/amazonS3.module'
import { FilesController } from './files.controller'
import { FilesService } from './files.service'
import { FilesSchema } from './types/files.schema'

const FilesModel = MongooseModule.forFeature([{
  schema: FilesSchema,
  name: 'Files',
}])

@Module({
  imports: [
    FilesModel,
    AwsModule,
  ],
  controllers: [FilesController],
  providers: [FilesService],
  exports: [FilesService],
})
export class FilesModule { }
