import { Document, Types } from 'mongoose'

export interface File extends Document {
  name: string
  s3Link: string
  fileSize?: number
  fileType?: string
  createdBy: Types.ObjectId
}

export interface CreateOneFileService {
  name: string
  s3Link: string
  fileSize?: number
  fileType?: string
  createdBy: Types.ObjectId
}

export interface FindOneFileQuery {
  _id: string
  name?: string
  createdBy: Types.ObjectId
}

interface UpdateFileData {
  name: string
}

export interface UpdateOneFileService {
  query: FindOneFileQuery
  data: UpdateFileData
}

export interface DeleteOneFileQuery {
  _id: string
  name?: string
  createdBy: Types.ObjectId
}

export interface UploadingFile {
  fileName?: string
  s3Link?: string
  fileSize?: number
  fileType?: string
}

export interface FindManyFilesQuery {
  name?: string
  s3Link?: string
  fileSize?: number
  fileType?: string
  createdBy?: Types.ObjectId
  limit?: number
  offset?: number
}

export interface FindManyFilesResult {
  data: File[]
  totalCount: number
}
