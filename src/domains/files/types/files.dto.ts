import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { IsNumber, IsOptional, IsString, Min } from 'class-validator'

export class CreateFileDto {
  @ApiProperty()
  @IsString()
  readonly name: string

  @ApiProperty()
  @IsString()
  readonly s3Link: string

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  readonly fileSize?: number

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly fileType?: string
}

export class UpdateFileDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly name: string
}

export class FindManyFilesDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly name?: string

  @ApiPropertyOptional()
  @IsNumber()
  @Min(0)
  @Type(() => Number)
  @IsOptional()
  readonly fileSize?: number

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly fileType?: string

  @ApiPropertyOptional()
  @IsNumber()
  @Min(1)
  @Type(() => Number)
  @IsOptional()
  readonly limit?: number = 10

  @ApiPropertyOptional()
  @IsNumber()
  @Min(0)
  @Type(() => Number)
  @IsOptional()
  readonly offset?: number = 0
}
