import * as mongoose from 'mongoose'
const { Types } = mongoose.Schema

export const FilesSchema = new mongoose.Schema({
  name: { type: String, required: true },
  fileSize: Number,
  s3Link: { type: String, required: true },
  fileType: String,
  createdBy: { type: Types.ObjectId, required: true, ref: 'Users' },
}, { timestamps: true })
