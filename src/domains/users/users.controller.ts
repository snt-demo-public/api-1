import { Body, Controller, Post, UseGuards, UsePipes, Request } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiTags } from '@nestjs/swagger'
import { AuthService } from '../../middlewares/auth/auth.service'
import { AccessToken } from '../../middlewares/auth/types/auth.interface'
import { ValidationPipe } from '../../middlewares/pipes/validation.pipe'
import { LoginDto, SignUpDto } from './types/users.dto'
import { UsersService } from './users.service'

@ApiTags('Users Controller')
@Controller()
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) { }

  @Post('auth/signup')
  @UsePipes(new ValidationPipe())
  async signup(
    @Body() data: SignUpDto,
  ): Promise<AccessToken> {
    try {
      const user = await this.usersService.createOne(data)

      const accessToken = await this.authService.generateJWT(user)

      return accessToken
    } catch (error) {
      throw error
    }
  }

  @Post('auth/login')
  @UseGuards(AuthGuard('local'))
  @UsePipes(new ValidationPipe())
  async login(
    @Request() { user },
    @Body() _: LoginDto,
  ): Promise<AccessToken> {
    try {
      const accessToken = await this.authService.generateJWT({ _id: user.userID })

      return accessToken
    } catch (error) {
      throw error
    }
  }
}
