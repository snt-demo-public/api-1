import { Injectable } from '@nestjs/common'
import { Model } from 'mongoose'
import * as bcrypt from 'bcrypt'
import { CreateUser, FindOneUserQuery, User } from './types/users.interface'
import { ConfigService } from '../../configs/configs.service'
import { InjectModel } from '@nestjs/mongoose'

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('Users')
    private readonly usersModel: Model<User>,
    private readonly configService: ConfigService,
  ) { }

  async createOne({ email, password }: CreateUser): Promise<User> {
    try {
      const hashedPassword = await bcrypt.hash(
        password,
        this.configService.bcryptSalt,
      )

      const user = new this.usersModel({
        email,
        password: hashedPassword,
      })

      await user.save()

      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async getCurrentUserCredentials(query: FindOneUserQuery): Promise<User> {
    try {
      const user = await this.usersModel.findOne(query).exec()

      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
