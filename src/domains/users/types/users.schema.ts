import * as mongoose from 'mongoose'

export const UsersSchema = new mongoose.Schema({
  email: { type: String, index: true, unique: true, required: true },
  password: { type: String, required: true },
}, { timestamps: true })
