import { Document, Types } from 'mongoose'

export interface FindOneUserQuery {
  email?: string
  _id?: Types.ObjectId
}

export interface User extends Document {
  email: string
  password: string
}

export interface CreateUser {
  email: string
  password: string
}
