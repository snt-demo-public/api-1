import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { ConfigModule } from '../../configs/configs.module'
import { AuthModule } from '../../middlewares/auth/auth.module'
import { UsersSchema } from './types/users.schema'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'

const UserModel = MongooseModule.forFeature([{ schema: UsersSchema, name: 'Users' }])

@Module({
  imports: [
    UserModel,
    ConfigModule,
    forwardRef(() => AuthModule),
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule { }
