import { Strategy } from 'passport-local'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { AuthService } from '../auth.service'
import { ReqUser } from '../types/auth.interface'

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
  ) {
    // we can pass passport's options to super()
    super({
      usernameField: 'email',
      passwordField: 'password',
    })
  }

  // returned object is bound to req.user
  async validate(email: string, password: string): Promise<ReqUser> {
    const userData = await this.authService.validateUser(email, password)

    if (!userData) {
      throw new UnauthorizedException()
    }

    return {
      userID: userData._id,
      email: userData.email,
    }
  }
}
