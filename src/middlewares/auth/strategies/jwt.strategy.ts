import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigService } from '../../../configs/configs.service'
import { ReqUser, TokenPayload } from '../types/auth.interface'
import { UsersService } from '../../../domains/users/users.service'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    configService: ConfigService,
    private readonly usersService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.jwtSecret,
    })
  }

  // returned object is bound to req.user
  async validate(payload: TokenPayload): Promise<ReqUser> {
    try {
      const userData = await this.usersService.getCurrentUserCredentials({ _id: payload.userID })

      if (!userData) {
        throw new UnauthorizedException()
      }

      return {
        userID: userData._id,
        email: userData.email,
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
