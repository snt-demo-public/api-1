import { Types } from 'mongoose'

export interface ReqUser {
  userID: Types.ObjectId
  email: string
}

export interface AccessToken {
  accessToken: string
}

export interface TokenPayload {
  userID: Types.ObjectId
}

export interface GenerateJWT {
  _id?: Types.ObjectId
}
