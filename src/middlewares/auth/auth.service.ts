import { Injectable, forwardRef, Inject } from '@nestjs/common'
import { UsersService } from '../../domains/users/users.service'
import { AccessToken, GenerateJWT, TokenPayload } from './types/auth.interface'
import * as bcrypt from 'bcrypt'
import * as jsonwebtoken from 'jsonwebtoken'
import { ConfigService } from '../../configs/configs.service'
import { User } from 'src/domains/users/types/users.interface'

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly configService: ConfigService,
  ) { }

  async validateUser(email: string, password: string): Promise<User | undefined> {
    const user = await this.usersService.getCurrentUserCredentials({ email })

    const isValid = user && await bcrypt.compare(password, user.password)

    if (isValid) {
      delete user.password

      return user
    }

    return null
  }

  async generateJWT(data: GenerateJWT): Promise<AccessToken> {
    const payload: TokenPayload = { userID: data._id }

    const accessToken = await jsonwebtoken.sign(
      payload,
      this.configService.jwtSecret,
      { expiresIn: '180d' },
    )

    return {
      accessToken,
    }
  }
}
