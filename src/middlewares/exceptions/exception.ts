import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common'
import { Response } from 'express'
import { getErrors } from '../../helpers/get_errors'

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()

    const errorObject =
      typeof exception.message === 'object' ?
      exception.message :
      exception

    const { code, message, error } = getErrors(errorObject)

    if (code === 401) {
      return response
        .status(code)
        .json({
          error,
          statusCode: code,
        })
    }

    return response
      .status(code)
      .json({
        error,
        statusCode: code,
        message: message || errorObject.message || exception.message,
      })
  }
}
