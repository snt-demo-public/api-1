import { Module } from '@nestjs/common'
import { ConfigModule } from './configs/configs.module'
import { FilesModule } from './domains/files/files.module'
import { UsersModule } from './domains/users/users.module'
import { AuthModule } from './middlewares/auth/auth.module'
import { DatabaseModule } from './third_parties/database/database.module'

@Module({
  imports: [
    ConfigModule,
    DatabaseModule,
    AuthModule,
    UsersModule,
    FilesModule,
  ],
})
export class AppModule { }
