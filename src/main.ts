import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { AppModule } from './app.module'
import { ConfigService } from './configs/configs.service'
import { HttpExceptionFilter } from './middlewares/exceptions/exception'

const configService = new ConfigService()

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule)

  app.enableCors()
  app.useGlobalFilters(new HttpExceptionFilter())

  // build documents for APIs
  const options = new DocumentBuilder()
    .setTitle('Interview Testing')
    .setDescription(`File Uploading APIs description`)
    .setVersion('1.0')
    .build()

  const document = SwaggerModule.createDocument(app, options)

  SwaggerModule.setup('api', app, document)

  await app.listen(configService.internalPort)

  // tslint:disable-next-line:no-console
  console.log(`our app started at ${configService.internalHost}:${configService.internalPort}`)
}

bootstrap()
