export const notFoundErrors = {
  FileNotFound: 'File not found',
}

export const badRequestErrors = {
  InvalidParams: 'Invalid Params',
}
