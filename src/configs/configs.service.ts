import * as dotenv from 'dotenv'
import * as Joi from 'joi'
import * as fs from 'fs'

export interface EnvConfig {
  [key: string]: string
}

export class ConfigService {
  private readonly envConfig: EnvConfig

  constructor() {
    const configFilePath = '.development.env'

    const config = dotenv.parse(fs.readFileSync(configFilePath))

    this.envConfig = this.validateInput(config)
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      DATABASE_USER: Joi.string().required(),
      DATABASE_PASSWORD: Joi.string().required(),
      DATABASE_HOST: Joi.string().required(),
      DATABASE_PORT: Joi.number().required(),
      DATABASE_NAME: Joi.string().required(),
      INTERNAL_HOST: Joi.string().required(),
      JWT_SECRET: Joi.string().required(),
      BCRYPT_SALT: Joi.string().required(),
      INTERNAL_PORT: Joi.string().required(),
      AWS_ACCESS_KEY: Joi.string().required(),
      AWS_SECRET_ACCESS_KEY: Joi.string().required(),
      AWS_BUCKET: Joi.string().required(),
      AWS_REGION: Joi.string().required(),
      AWS_FOLDER: Joi.string().required(),
    })

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(envConfig)

    if (error) {
      throw new Error(`Config validation error: ${error.message}`)
    }

    return validatedEnvConfig
  }

  get bcryptSalt(): number {
    return Number(this.envConfig.BCRYPT_SALT)
  }

  get jwtSecret(): string {
    return String(this.envConfig.JWT_SECRET)
  }

  get internalHost(): string {
    return String(this.envConfig.INTERNAL_HOST)
  }

  get internalPort(): string {
    return String(this.envConfig.INTERNAL_PORT)
  }

  get jwtComfirmSecret(): string {
    return String(this.envConfig.JWT_COMFIRM_SECRET)
  }

  get awsAccessKey(): string {
    return String(this.envConfig.AWS_ACCESS_KEY)
  }

  get awsSecretAccessKey(): string {
    return String(this.envConfig.AWS_SECRET_ACCESS_KEY)
  }

  get awsBucket(): string {
    return String(this.envConfig.AWS_BUCKET)
  }

  get awsRegion(): string {
    return String(this.envConfig.AWS_REGION)
  }

  get awsFolder(): string {
    return String(this.envConfig.AWS_FOLDER)
  }

  get dbURI(): string {
    // tslint:disable-next-line:max-line-length
    return `mongodb+srv://${this.envConfig.DATABASE_USER}:${this.envConfig.DATABASE_PASSWORD}@${this.envConfig.DATABASE_HOST}/${this.envConfig.DATABASE_NAME}?retryWrites=true&w=majority`
  }
}
